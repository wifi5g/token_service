import binascii
import ConfigParser
import logging
import logging.handlers
import os
from flask import Flask, request

from service import api
from utility.mongo_storage import create_mongo_url, MongoStorage


def _read_configuration():
    parser = ConfigParser.RawConfigParser()
    parser.read('settings.ini')

    config = {}
    for section in parser.sections():
        config[section] = {}
        for option in parser.options(section):
            config[section][option] = parser.get(section, option)
    return config


def _initialize_logging(app):
    log_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs')
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    handler = logging.handlers.RotatingFileHandler(os.path.join(log_dir, 'service.log'),
                                                   maxBytes=(10 * 1024 * 1024), backupCount=10)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s][%(process)d/%(threadName)s] %(message)s [%(filename)s:%(lineno)d]')
    handler.setFormatter(formatter)

    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(handler)


def create_app(database=None):

    app = Flask(__name__)
    app.register_blueprint(api)

    @app.before_request
    def dump_request():
        content_type = request.headers.get('Content-Type', '')
        if content_type == 'application/x-www-form-urlencoded':
            data = request.form
        elif content_type == 'application/json':
            data = request.get_json()
        elif request.method == 'GET':
            data = request.args
        elif request.method == 'POST':
            data = request.form
        else:
            data = request.data
        app.logger.info(u"{0} {1}: Headers - '{2}', Data - {3}".format(
            request.method, request.path, request.headers, data))

    _initialize_logging(app)
    app.logger.info("Start service ...")

    # Database
    app.database = database
    if not app.database:

        # Read configuration
        config = _read_configuration()
        db_config = config['mongo_db']

        # Connect to database
        url = create_mongo_url(db_config)
        app.database = MongoStorage(url, db_config['database'], db_config['collection'],
                                    reconnect_attempts=5, logger=app.logger)

    # Save root token
    if not app.database.find_one(query={'user_name': 'super'}):
        super_token = binascii.hexlify(os.urandom(16))
        app.database.insert_one(document={'user_name': 'super', 'token': super_token,
                                          'access': [{'object': '*', 'admin': 'true', 'aux': {}}]})

    # Create indexes
    if not app.database.find_index(field='user_name'):
        app.database.create_index(fields=['user_name'], unique=True)
    if not app.database.find_index(field='token'):
        app.database.create_index(fields=['token'], unique=True)

    return app

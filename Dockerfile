FROM ubuntu
MAINTAINER Alexey Korshunov <korshunovmail@mail.ru>

ENV main_path /opt/token_service
RUN apt-get update && \
    apt-get -y install python-pip python-dev && \
    pip install --upgrade pip && \
    pip install flask pymongo tornado requests

EXPOSE 5000
VOLUME $main_path
WORKDIR $main_path

CMD python server.py

import binascii
import os
import traceback
from flask import Blueprint, request
from flask import current_app as app

from utility.error import Error, BadRequestToken, BadRequestData, NotFoundResource
from utility.mongo_storage import DuplicateError
from utility.service import get_json, get_param, make_result
from utility.token_access import check_auth_token, check_access

api = Blueprint('api', __name__)
mandatory_fields = ['object', 'admin']

service_token = '4d682e692913d7bc911ea3d2532fe6e4'


class NotFoundToken(NotFoundResource):
    def __init__(self, query):
        NotFoundResource.__init__(self, "Not found token by filter '{0}'".format(query))


class NotFoundAuthToken(BadRequestToken):
    def __init__(self, query):
        BadRequestToken.__init__(self, "Not found auth token by filter '{0}'".format(query))


def _verify_access(access):
    for field in mandatory_fields:
        if field not in access:
            raise BadRequestData("Not found '{0}' parameter in access object".format(field))


def _modify_access(access):
    # Modify aux info
    excludes = mandatory_fields + ['aux']
    aux_fields = {k: v for k, v in access.items() if k not in excludes}
    for k, v in access.items():
        if k not in excludes:
            del access[k]
    if 'aux' not in access:
        access['aux'] = {}
    if aux_fields:
        access['aux'].update(aux_fields)

    # Modify 'admin' field
    admin = access['admin']
    if admin == 'true' or admin == 'True' or admin == 'TRUE' or admin is True:
        access['admin'] = 'true'
    else:
        access['admin'] = 'false'


def _check_service_token(request):
    if request.headers['auth_token'] != service_token:
        raise BadRequestToken("Invalid auth token")


@api.before_request
def wrap_check_auth_token():
    check_auth_token(request)


@api.errorhandler(Exception)
def handle_error(e):
    status_code = e.http_code if isinstance(e, Error) else 500
    summary = e.summary if isinstance(e, Error) else "internal"
    return make_result(summary, status_code, traceback.format_exc())


@api.route('/tokens/create', methods=['POST'])
def create_token():

    # Get data content
    data = get_json(request)
    if not data:
        raise BadRequestData("Empty input data")

    # Get user name
    user_name = get_param(data, 'user_name')

    # Get and verify access list
    access_list = get_param(data, 'access')
    for access in access_list:
        _verify_access(access)
        _modify_access(access)

    # Read information about current token
    root_doc = app.database.find_one({'token': request.headers['auth_token']}, exception_cls=NotFoundAuthToken)

    # Check access rights
    objects = [access['object'] for access in access_list]
    check_access(root_doc['access'], required_objects=objects, admin=True)

    new_token = binascii.hexlify(os.urandom(16))
    try:
        app.database.insert_one(document={'user_name': user_name, 'token': new_token, 'access': access_list})
    except DuplicateError:
        raise BadRequestData("Token already exists for user '{0}'".format(user_name))

    return make_result(new_token, 200)


@api.route('/tokens/delete', methods=['POST'])
def delete_token():

    # Token to delete
    token = get_param(request.form, 'token')

    # Check access rights
    root_doc = app.database.find_one({'token': request.headers['auth_token']}, exception_cls=NotFoundAuthToken)
    doc = app.database.find_one({'token': token}, exception_cls=NotFoundToken)
    objects = [access['object'] for access in doc['access']]
    check_access(root_doc['access'], required_objects=objects, admin=True)

    # Delete from database
    app.database.delete_one({'token': token})

    return make_result("success", 200)


@api.route('/tokens/get', methods=['GET'])
def get_token():

    def _filter_result(result):
        del result['user_name']
        del result['_id']

    _check_service_token(request)

    # Get user name
    user_name = get_param(request.args, 'user_name')

    # Find in database
    doc = app.database.find_one({'user_name': user_name}, exception_cls=NotFoundToken)

    _filter_result(doc)
    return make_result(doc, 200)


@api.route('/tokens/get_access', methods=['GET'])
def get_access():

    _check_service_token(request)

    # Get target token
    token = get_param(request.args, 'token')

    # Find in database
    doc = app.database.find_one({'token': token}, exception_cls=NotFoundToken)

    return make_result(doc['access'], 200)

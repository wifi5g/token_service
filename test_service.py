#!/usr/bin/env python

import binascii
import json
import os
import unittest

from app import create_app
from service import service_token
from utility.log import create_stdout_logger
from utility.mongo_storage import create_mongo_url, MongoStorage
from utility.flask_test import FlaskAppTest

URL = 'http://localhost:5000/tokens'

_logger = create_stdout_logger(__name__)

_db_config = {
    'host1': 'db1.wifi5g.ru',
    'host2': 'db2.wifi5g.ru',
    'user': 'test_captive',
    'password': 'test_!WD3wa2ws',
    'replica_set': 'wifi5g_replset',
    'database': 'test_wifi5g',
    'collection': 'tokens'
}


class TokensTest(FlaskAppTest, unittest.TestCase):

    def setUp(self):
        url = create_mongo_url(_db_config)
        self.database = MongoStorage(url, _db_config['database'], _db_config['collection'], reconnect_attempts=5, logger=_logger)
        self.app = create_app(self.database)
        self.client = self.app.test_client()

        # Get root token
        r = self.get(URL + '/get', data={'user_name': 'super'}, headers={'auth_token': service_token}, expect=200)
        self.root_token = r['result']['token']

    def tearDown(self):
        self.database.clear()
        self.database.drop()


class CreateTokenTest(TokensTest):
    """
    Tests for '/create' endpoint
    """
    def test_error_without_auth_token(self):
        r = self.post(URL + '/create', data={}, headers={}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

    def test_error_with_not_json_mimetype(self):
        r = self.post(URL + '/create', data={}, headers={'auth_token': binascii.hexlify(os.urandom(16))}, expect=400)
        self.assertTrue(r['result'] == "invalid mimetype")

    def test_error_with_empty_json_data(self):
        r = self.post(URL + '/create', data=json.dumps({}), headers={'auth_token': binascii.hexlify(os.urandom(16))},
                      content_type='application/json', expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_error_with_empty_user_name(self):
        r = self.post(URL + '/create', data=json.dumps({'access': [{'object': 'RU02-001', 'admin': True}]}),
                      headers={'auth_token': binascii.hexlify(os.urandom(16))},
                      content_type='application/json', expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_error_with_empty_access_list(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1'}),
                      headers={'auth_token': binascii.hexlify(os.urandom(16))},
                      content_type='application/json', expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_error_with_invalid_access_list(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'admin': True, 'aux_param1': 'value'}]}),
                      headers={'auth_token': binascii.hexlify(os.urandom(16))},
                      content_type='application/json', expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_error_not_found_auth_token(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True, 'aux_param1': 'value'}]}),
                      headers={'auth_token': binascii.hexlify(os.urandom(16))},
                      content_type='application/json', expect=403)
        self.assertTrue(r['result'] == "invalid token")

    def test_success_with_root_token(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True, 'aux_param1': 'value'}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        self.assertTrue(r['result'] != "")

    def test_error_with_existing_token(self):
        self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True, 'aux_param1': 'value'}]}),
                  headers={'auth_token': self.root_token},
                  content_type='application/json', expect=200)
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True, 'aux_param1': 'value'}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_access_denied_to_other_object(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        new_token = r['result']
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user2', 'access': [{'object': 'RU24-001', 'admin': True}]}),
                      headers={'auth_token': new_token},
                      content_type='application/json', expect=403)
        self.assertTrue(r['result'] == "access denied")

    def test_access_denied_with_non_admin(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': 'false'}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        new_token = r['result']
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user2', 'access': [{'object': 'RU02-001', 'admin': 'true'}]}),
                      headers={'auth_token': new_token},
                      content_type='application/json', expect=403)
        self.assertTrue(r['result'] == "access denied")


class DeleteTokenTest(TokensTest):
    """
    Tests for '/delete' endpoint
    """
    def test_error_without_auth_token(self):
        r = self.post(URL + '/delete', data={}, headers={}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

    def test_error_without_target_token(self):
        r = self.post(URL + '/delete', data={}, headers={'auth_token': self.root_token}, expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_error_with_invalid_auth_token(self):
        r = self.post(URL + '/delete', data={'token': self.root_token},
                      headers={'auth_token': binascii.hexlify(os.urandom(16))}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

    def test_error_with_invalid_target_token(self):
        r = self.post(URL + '/delete', data={'token': '0afe436a8ce5799ec97e11b08b23a3b4'},
                      headers={'auth_token': self.root_token}, expect=404)
        self.assertTrue(r['result'] == "not found")

    def test_success_delete_token(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': True}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        user_token = r['result']
        r = self.post(URL + '/delete', data={'token': user_token}, headers={'auth_token': self.root_token}, expect=200)
        self.assertTrue(r['result'] == "success")


class ManageTokensBySubAdmins(TokensTest):
    """
    Tests 'create/delete' endpoints for 2nd layer of administrators
    """
    def setUp(self):
        TokensTest.setUp(self)
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'admin1', 'access': [{'object': 'RU02-001', 'admin': True}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        self.admin1_token = r['result']
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'admin2', 'access': [{'object': 'RU02-001', 'admin': True}, {'object': 'RU24-001', 'admin': True}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        self.admin2_token = r['result']

    def test_create_sub_token(self):
        self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU02-001', 'admin': False}]}),
                  headers={'auth_token': self.admin1_token},
                  content_type='application/json', expect=200)

        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user2', 'access': [{'object': 'RU24-001', 'admin': False}]}),
                      headers={'auth_token': self.admin1_token},
                      content_type='application/json', expect=403)
        self.assertTrue(r['result'] == "access denied")

    def test_delete_sub_token(self):
        r = self.post(URL + '/create', data=json.dumps({'user_name': 'user1', 'access': [{'object': 'RU24-001', 'admin': False}]}),
                      headers={'auth_token': self.admin2_token},
                      content_type='application/json', expect=200)
        user_token = r['result']

        r = self.post(URL + '/delete', data={'token': user_token}, headers={'auth_token': self.admin1_token}, expect=403)
        self.assertTrue(r['result'] == "access denied")

        r = self.post(URL + '/delete', data={'token': user_token}, headers={'auth_token': self.admin2_token}, expect=200)
        self.assertTrue(r['result'] == "success")


class GetTokenBaseTest(TokensTest):
    """
    Base test class for 'get/get_access' endpoints
    """
    def setUp(self):
        TokensTest.setUp(self)
        self.user_name = 'admin'
        r = self.post(URL + '/create',
                      data=json.dumps({'user_name': self.user_name, 'access': [{'object': 'RU02-001', 'admin': True}]}),
                      headers={'auth_token': self.root_token},
                      content_type='application/json', expect=200)
        self.user_token = r['result']


class AuthTokenForGetTokenTest(GetTokenBaseTest):
    """
    Tests for 'auth_token' header
    """
    def test_forbidden_without_auth_token(self):
        r = self.get(URL + '/get', data={'user_name': self.user_name}, headers={}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

        r = self.get(URL + '/get_access', data={'token': self.user_token}, headers={}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

    def test_forbidden_with_invalid_auth_token(self):
        auth_token = binascii.hexlify(os.urandom(16))
        r = self.get(URL + '/get', data={'user_name': self.user_name}, headers={'auth_token': auth_token}, expect=403)
        self.assertTrue(r['result'] == "invalid token")

        r = self.get(URL + '/get_access', data={'token': self.user_token}, headers={'auth_token': auth_token}, expect=403)
        self.assertTrue(r['result'] == "invalid token")


class GetTokenTest(GetTokenBaseTest):
    """
    Tests for '/get' endpoint
    """
    def test_fail_without_user_name(self):
        r = self.get(URL + '/get', data={}, headers={'auth_token': service_token}, expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_fail_with_invalid_user_name(self):
        r = self.get(URL + '/get', data={'user_name': 'user'}, headers={'auth_token': service_token}, expect=404)
        self.assertTrue(r['result'] == "not found")

    def test_successful_get_token(self):
        r = self.get(URL + '/get', data={'user_name': self.user_name}, headers={'auth_token': service_token}, expect=200)
        self.assertIn('token', r['result'])
        self.assertIn('access', r['result'])


class GetAccessTest(GetTokenBaseTest):
    """
    Tests for '/get_access' endpoint
    """
    def test_fail_without_token(self):
        r = self.get(URL + '/get_access', data={}, headers={'auth_token': service_token}, expect=400)
        self.assertTrue(r['result'] == "invalid data")

    def test_fail_with_invalid_token(self):
        token = binascii.hexlify(os.urandom(16))
        r = self.get(URL + '/get_access', data={'token': token}, headers={'auth_token': service_token}, expect=404)
        self.assertTrue(r['result'] == "not found")

    def test_successful_get_access(self):
        r = self.get(URL + '/get_access', data={'token': self.user_token}, headers={'auth_token': service_token}, expect=200)
        self.assertIsInstance(r['result'], list)


if __name__ == "__main__":
    unittest.main()
